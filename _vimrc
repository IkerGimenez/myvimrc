set nocompatible              " be iMproved, required

set pythonthreehome=C:\\Users\\ikerb\\.pyenv\\pyenv-win\\versions\\3.9.6\\
set pythonthreedll=C:\\Users\\ikerb\\.pyenv\\pyenv-win\\versions\\3.9.6\\python39.dll

" Specify a directory for plugins
" - For Neovim: ~/.local/share/nvim/plugged
" - Avoid using standard Vim directory names like 'plugin'
call plug#begin('$HOME\vimfiles\plugged')

Plug 'nanotech/jellybeans.vim'
Plug 'vim-scripts/autumnleaf_modified.vim'
Plug 'tpope/vim-sensible'
Plug 'bfrg/vim-cpp-modern'
Plug 'jansedivy/jai.vim'
Plug 'PProvost/vim-ps1'
Plug 'sonph/onehalf', { 'rtp': 'vim' }
Plug 'liuchengxu/vista.vim'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'skywind3000/asyncrun.vim'
Plug '~/vimfiles/plugged/TagHighlight'
Plug 'whiteinge/diffconflicts'
Plug 'junegunn/fzf', {'dir': '~/.fzf','do': './install --all'}
Plug 'junegunn/fzf.vim' " needed for previews
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'antoinemadec/coc-fzf', {'branch': 'release'}
" Plug 'ngemily/vim-vp4'

call plug#end()

if !isdirectory('%USERPROFILE%\vimfiles\undo-dir')
    if exists("*mkdir")
        call mkdir('%USERPROFILE%\vimfiles\undo-dir') 
    endif
endif

set undodir="%USERPROFILE%\vimfiles\undo-dir"
set undofile

" font and colorscheme
set guifont=Fira_Code_Medium:h9:W500:cANSI:qDRAFT
colorscheme onehalflight

" airline color scheme
let g:airline_theme='onehalflight'

" enable syntax highlighting
syntax enable

" enable line numbers
set number

" tabs as spaces
set tabstop=4
set shiftwidth=4
set softtabstop=4
set expandtab
set smarttab

" Indentation options
set cino=g0 " Do not indent access specifier scopes (private, public, protected)

" enable utf-8 encoding for YCM
set encoding=utf-8
set rop=type:directx,gamma:1.0,contrast:0.5,level:1,geom:1,renmode:4,taamode:1

let g:airline_powerline_fonts = 1

" coc settings file
source ~\cocnvim

" allow to scroll in the fzf preview
set mouse=a

" coc-fzf mappings
nnoremap <silent> <space><space> :<C-u>CocFzfList<CR>
nnoremap <silent> <space>a       :<C-u>CocFzfList diagnostics<CR>
nnoremap <silent> <space>b       :<C-u>CocFzfList diagnostics --current-buf<CR>
nnoremap <silent> <space>c       :<C-u>CocFzfList commands<CR>
nnoremap <silent> <space>e       :<C-u>CocFzfList extensions<CR>
nnoremap <silent> <space>l       :<C-u>CocFzfList location<CR>
nnoremap <silent> <space>o       :<C-u>CocFzfList outline<CR>
nnoremap <silent> <space>s       :<C-u>CocFzfList symbols<CR>
nnoremap <silent> <space>p       :<C-u>CocFzfListResume<CR>

" FZF floating window
let g:fzf_layout = { 'window': { 'width': 0.9, 'height': 0.6 } }
let g:fzf_preview_window = ['right:50%:hidden', 'ctrl-/']

" Search down into subfolders
" Provides tab-completion for all file related tasks
set path+=**

" Display matching files when tab completing
set wildmenu

" FILE BROWSING:

" Tweaks for browsing
let g:netrw_banner=0        " disable annoying banner
let g:netrw_browse_split=4  " open in prior window
let g:netrw_altv=1          " open splits to the right
let g:netrw_liststyle=3     " tree view
let g:netrw_list_hide=''

" Easier tab navigation
nnoremap <C-Left> :tabprevious<CR>
nnoremap <C-Right> :tabnext<CR>

" TAG JUMPING:

" Create the `tags` file (may need to install ctags first)
command! MakeTags !ctags -R --fields=+l .

" NOW WE CAN:
" - Use ^] to jump to tag under cursor
" - Use g^] for ambiguous tags
" - Use ^t to jump back up the tag stack

" THINGS TO CONSIDER:
" - This doesn't help if you want a visual list of tags

" Create commands to switch to lighmode and darkmode
command DarkMode colorscheme jellybeans | AirlineTheme jellybeans
command LightMode colorscheme onehalflight | AirlineTheme onehalflight

"augroup JsonToJsonc
    "autocmd! FileType json set filetype=jsonc
"augroup END
